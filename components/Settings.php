<?php

namespace app\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use app\models\Settings as ActiveRecordSettings;

/**
 * Class Settings
 * @package app\components
 */
class Settings extends Component
{
    private $_settings;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->_settings = ArrayHelper::map(ActiveRecordSettings::find()->all(), 'key', 'value');
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if(isset($this->_settings[$name])){
            return $this->_settings[$name];
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        $model = ActiveRecordSettings::findByKey($name);

        if($model){
            $model->value = $value;
            $model->save(false);
        }
    }
}