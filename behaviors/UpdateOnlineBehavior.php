<?php

namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\Controller;
use app\models\User;
use yii\base\Event;
use app\models\Logs;

/**
 * Class UpdateOnlineBehavior
 * @package app\models\behaviors
 */
class UpdateOnlineBehavior extends Behavior
{
    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            Controller::EVENT_AFTER_ACTION => 'setUserOnline',
        ];
    }

    /**
     * @param Event $event
     * @return null|bool
     */
    public function setUserOnline($event)
    {
         if(Yii::$app->user->isGuest){
            return false;
         }

        /** @var User $user */
         $user = Yii::$app->user->identity;

         $user->last_active_datetime = date('Y-m-d H:i:s');

         $user->save(false);
    }
}