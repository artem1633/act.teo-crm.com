<?php

use yii\db\Migration;

/**
 * Class m200318_063356_table_operations
 */
class m200318_063356_table_operations extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('operations', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('ФИО'),
            'order' => $this->string()->comment('Продукт'),
            'sum' => $this->float()->unsigned()->comment('Сумма'),
            'user_id' => $this->float()->unsigned()->comment('Сумма'),
            'company_id' => $this->float()->unsigned()->comment('Сумма'),
            'created_at' => $this->dateTime()->comment('Дата и время создания'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('operations');
    }
}
