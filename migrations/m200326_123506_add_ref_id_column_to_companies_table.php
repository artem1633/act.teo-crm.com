<?php

use yii\db\Migration;

/**
 * Handles adding ref_id to table `companies`.
 */
class m200326_123506_add_ref_id_column_to_companies_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('companies', 'ref_id', $this->integer()->comment('Реф'));

        $this->createIndex(
            'idx-companies-ref_id',
            'companies',
            'ref_id'
        );

        $this->addForeignKey(
            'fk-companies-ref_id',
            'companies',
            'ref_id',
            'companies',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-companies-ref_id',
            'companies'
        );

        $this->dropIndex(
            'idx-companies-ref_id',
            'companies'
        );

        $this->dropColumn('companies', 'ref_id');
    }
}
