<?php

use yii\db\Migration;

/**
 * Class m200318_062351_table_company
 */
class m170418_140712_table_company extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('companies', [
            'id' => $this->primaryKey(),
            'company_name' => $this->string(255)->comment('Название компании'),
            'created_date' => $this->date()->comment('Дата регистрации'),
            'rate_a' => $this->float()->comment('Тариф A'),
            'rate_b' => $this->float()->comment('Тариф B'),
            'rate_c' => $this->float()->comment('Тариф C'),
            'rate_d' => $this->float()->comment('Тариф D'),
            'credit' => $this->float()->comment('Кредитов'),
            'last_activity_datetime' => $this->dateTime()->comment('Дата и время последней активности'),
            'access_end_datetime' => $this->datetime()->comment('Дата и время потери доступа к системе'),
            'is_super' => $this->boolean()->defaultValue(0)->comment('Супер компания'),
            'is_activity' => $this->boolean()->defaultValue(1)->comment('Доступ'),
        ]);

        $this->addCommentOnTable('companies', 'Компании');


        $this->insert('companies', [
            'company_name' => 'admin',
            'is_super' => 1,
            'is_activity' => 1,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('companies');
    }
}
