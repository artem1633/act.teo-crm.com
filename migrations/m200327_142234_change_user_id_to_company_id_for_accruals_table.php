<?php

use yii\db\Migration;

/**
 * Class m200327_142234_change_user_id_to_company_id_for_accruals_table
 */
class m200327_142234_change_user_id_to_company_id_for_accruals_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->dropForeignKey(
            'fk-accruals-for_user_id',
            'accruals'
        );

        $this->dropIndex(
            'idx-accruals-for_user_id',
            'accruals'
        );

        $this->dropForeignKey(
            'fk-accruals-from_user_id',
            'accruals'
        );

        $this->dropIndex(
            'idx-accruals-from_user_id',
            'accruals'
        );

        $this->dropColumn('accruals', 'from_user_id');
        $this->dropColumn('accruals', 'for_user_id');

        $this->addColumn('accruals', 'from_company_id', $this->integer()->comment('Откуда'));
        $this->addColumn('accruals', 'for_company_id', $this->integer()->comment('Куда'));

        $this->createIndex(
            'idx-accruals-from_company_id',
            'accruals',
            'from_company_id'
        );

        $this->addForeignKey(
            'fk-accruals-from_company_id',
            'accruals',
            'from_company_id',
            'companies',
            'id',
            'SET NULL'
        );

        $this->createIndex(
            'idx-accruals-for_company_id',
            'accruals',
            'for_company_id'
        );

        $this->addForeignKey(
            'fk-accruals-for_company_id',
            'accruals',
            'for_company_id',
            'companies',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-accruals-for_company_id',
            'accruals'
        );

        $this->dropIndex(
            'idx-accruals-for_company_id',
            'accruals'
        );

        $this->dropForeignKey(
            'fk-accruals-from_company_id',
            'accruals'
        );

        $this->dropIndex(
            'idx-accruals-from_company_id',
            'accruals'
        );

        $this->dropColumn('accruals', 'from_company_id');
        $this->dropColumn('accruals', 'for_company_id');
    }
}
