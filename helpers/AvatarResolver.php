<?php

namespace app\helpers;

/**
 * Class AvatarResolver
 * @package app\helpers
 */
class AvatarResolver
{
    /**
     * @param string $path Путь к аватару
     * @param bool $for_chat Аватар для чата или нет
     * @return string
     */
    public static function getRealAvatarPath($path, $for_chat = false)
    {
        if (is_file($path)){
            return $path;
        } else {
            if (!$for_chat){
                return 'img/nouser.png';
//                return $path != null ? $path : 'img/nouser.png';
            } else {
//                return $path != null ? $path : 'img/nouser_chat.png';
                return 'img/nouser_chat.png';
            }
        }
    }
}