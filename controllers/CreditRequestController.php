<?php

namespace app\controllers;

use Yii;
use app\behaviors\UpdateOnlineBehavior;
use app\models\forms\CreditRequestForm;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\Html;

/**
 * Class CreditRequestController
 * @package app\controllers
 */
class CreditRequestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            ['class' => UpdateOnlineBehavior::className()],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [

        ]);
    }

    public function actionRequest()
    {
        $request = Yii::$app->request;
        $model = new CreditRequestForm();

        Yii::$app->response->format = Response::FORMAT_JSON;
        if($request->isGet){
            return [
                'title'=> "Запрос",
                'content'=>$this->renderAjax('send', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

            ];
        }else if($model->load($request->post()) && $model->send()){
            return [
                'title'=> "Запрос",
                'content'=>'<span class="text-success">Запрос успешно отправлен</span>',
                'footer'=> Html::button('ОК',['class'=>'btn btn-block btn-default pull-left','data-dismiss'=>"modal"])
            ];
        }else{
            return [
                'title'=> "Запрос",
                'content'=>$this->renderAjax('send', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Создать',['class'=>'btn btn-primary','type'=>"submit"])

            ];
        }
    }
}