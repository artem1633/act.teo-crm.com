<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Авторизация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];
?>


<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span> <?=Yii::$app->name?>
            <small>Введите данные для авторизации</small>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">
        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
        <?php if(Yii::$app->session->hasFlash('success')): ?>
            <b class="text-success"><?= Yii::$app->session->getFlash('success') ?></b>
        <?php endif; ?>
        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['value'=>'admin','placeholder' => $model->getAttributeLabel('email'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['value'=>'admin','placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg no-border']) ?>
        <div class="text-center" style="margin-top: 20px; margin-bottom: 10px; text-align: center;">
            <?= Html::a('Забыли пароль', ['site/forget-password'], ['class' => 'pull-left']) ?>
            <?= Html::a('Регистрация', ['site/register'], ['class' => 'pull-right']) ?>
        </div>
        <div class="login-buttons" style="margin-top: 50px; text-align: center;">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-success btn-lg', 'name' => 'login-button', 'style' => 'width: 150px;']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>



