<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Регистрация';

$fieldOptions1 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group m-b-20'],
    'inputTemplate' => "{input}"
];

if(isset($_GET['ref_id'])){
    $refId = $_GET['ref_id'];
} else {
    $refId = null;
}


$model->ref_id = $refId;

?>


<div class="login bg-black animated fadeInDown">
    <!-- begin brand -->
    <div class="login-header">
        <div class="brand">
            <span class="logo"></span> <?=Yii::$app->name?>
            <small>Регистрация</small>
        </div>
        <div class="icon">
            <i class="fa fa-sign-in"></i>
        </div>
    </div>
    <!-- end brand -->
    <div class="login-content">
        <?php $form = ActiveForm::begin(['id' => 'register-form', 'enableClientValidation' => false], $options = ['class' => 'margin-bottom-0']); ?>
        <?= $form
            ->field($model, 'name', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('name'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'email', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('email'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'birth_date', $fieldOptions2)
            ->label()
            ->textInput(['type' => 'date'],['placeholder' => $model->getAttributeLabel('birth_date'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'country', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('country'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'living_country', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('living_country'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'living_place', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('living_place'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'education', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('education'), 'class' => 'form-control input-lg no-border']) ?>
         <?= $form
            ->field($model, 'profession', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('profession'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'work_place', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('work_place'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'work_position', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('work_position'), 'class' => 'form-control input-lg no-border']) ?>
         <?= $form
            ->field($model, 'work_status', $fieldOptions2)
            ->label($model->getAttributeLabel('work_status'))
            ->radioList(['да' => 'да',
                            'нет' => 'нет',
                            'возможно' => 'возможно',
            ]) ?>
        <?= $form
            ->field($model, 'phone', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('phone'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'interests', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('interests'), 'class' => 'form-control input-lg no-border']) ?>
        <?= $form
            ->field($model, 'hobby', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('hobby'), 'class' => 'form-control input-lg no-border']) ?>

        <?= $form
            ->field($model, 'ref_id', $fieldOptions2)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('ref_id'), 'class' => 'form-control input-lg no-border']) ?>


        <div class="login-buttons">
            <?= Html::submitButton('Регистрация', ['class' => 'btn btn-success btn-block btn-lg', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

