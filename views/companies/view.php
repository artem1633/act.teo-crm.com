<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
?>
<div class="companies-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'company_name',
            'created_date',
            'rate_a',
            'rate_b',
            'rate_c',
            'rate_d',
            'credit',
            'last_activity_datetime',
            'access_end_datetime',
            'is_super',
            'is_activity',
        ],
    ]) ?>

</div>
