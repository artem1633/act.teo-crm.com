<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Companies */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="companies-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'company_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_date')->textInput() ?>

    <?= $form->field($model, 'rate_a')->textInput() ?>

    <?= $form->field($model, 'rate_b')->textInput() ?>

    <?= $form->field($model, 'rate_c')->textInput() ?>

    <?= $form->field($model, 'rate_d')->textInput() ?>

    <?= $form->field($model, 'credit')->textInput() ?>

    <?= $form->field($model, 'last_activity_datetime')->textInput() ?>

    <?= $form->field($model, 'access_end_datetime')->textInput() ?>

    <?= $form->field($model, 'is_super')->textInput() ?>

    <?= $form->field($model, 'is_activity')->textInput() ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
