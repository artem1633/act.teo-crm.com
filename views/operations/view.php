<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Operations */
?>
<div class="operations-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'order',
            'sum',
            'user_id',
            'company_id',
            'created_at',
        ],
    ]) ?>

</div>
