<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Operations */
?>
<div class="operations-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
