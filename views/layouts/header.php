<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Accruals;
use app\models\Operations;

/** @var $this \yii\web\View */

$balanceHtml = '';

if(Yii::$app->user->isGuest == false){
    $now = date('Y-m-d');
    $lastMonth = date('Y-m-d', time() - (86400 * 30));
    if(Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isManager()){
        $debit = Yii::$app->user->identity->company->credit;
        $balanceHtml = "Кредиты: {$debit}";
    } else if(Yii::$app->user->identity->isShop()){
        $debit = array_sum(ArrayHelper::getColumn(Operations::find()->where(['user_id' => Yii::$app->user->identity->id])->andWhere(['between', 'created_at', "{$lastMonth} 00:00:00", "{$now} 23:59:59"])->all(), 'sum'));
        $balanceHtml = "Оборот: {$debit}";
    }
}

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top" style="min-height: unset; height: 41px;">
            <!-- begin container-fluid -->
            <div class="container-fluid">
                <!-- begin mobile sidebar expand / collapse button -->
                <div class="navbar-header">
                    <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                        <span class="navbar-logo"></span>
                        <?=Yii::$app->name?>
                    </a>
                    <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- end mobile sidebar expand / collapse button -->
                <?php
                if (Yii::$app->controller->id != 'chat'): ?>
                <ul class="header-menu">
                </ul>
                <?php endif; ?>
                <?php if(Yii::$app->user->isGuest == false): ?>
                    <!-- begin header navigation right -->
                    <ul class="nav navbar-nav navbar-right" style="
    background: #f0da58;
    height: 41px;">
                        <li class="dropdown navbar-user">
                            <a id="btn-dropdown_header" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="/<?= Yii::$app->user->identity->getRealAvatarPath() ?>" data-role="avatar-view" alt="">
                                <?php
                                    $login = Yii::$app->user->identity->login;

                                    if(strlen($login) > 15){
                                        $login = iconv_substr ($login, 0, 13, "UTF-8").'...';
                                    }
                                ?>
                                <span class="hidden-xs"><?=$login?></span> <b class="caret"></b>
                                <span style="margin-left: 10px;"><?= $balanceHtml ?></span>
                            </a>
                            <div style="text-align: right;">
                                <ul class="dropdown-menu animated fadeInLeft">
                                    <li> <?= Html::a('Счет', ['accruals/index']) ?> </li>
                                    <li> <?= Html::a('Профиль', ['user/profile']) ?> </li>
                                    <li> <?= Html::a('Выйти', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                                </ul>
                            </div>
                        </li>
                    </ul>

<!--                    <ul class="nav navbar-nav navbar-right">-->
<!--                        <li class="dropdown navbar-user" style="margin-top: 12px;">-->
<!--                            <p>--><?php //echo Yii::$app->user->identity->selfRefHref;?><!--</p>-->
<!--                        </li>-->
<!--                    </ul>-->
                    <!-- end header navigation right -->
                <?php endif; ?>
            </div>
            <!-- end container-fluid -->
        </div>