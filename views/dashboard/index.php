<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $dataProvider yii\data\ArrayDataProvider */

$this->title = 'Главная';

\app\assets\plugins\EqualsHeightAsset::register($this);

$pages = new \yii\data\Pagination(['totalCount' => $dataProvider->count]);

?>

<div class="col-md-12">
    <?php $form = ActiveForm::begin(['id' => 'user-main-form', 'method' => 'GET']); ?>

    <?= $form->field($searchModel, 'sorting')->radioList(
        ['1' => 'По чел.', '2' => 'По $'],
        [
            'item' => function($index, $label, $name, $checked, $value){
                return "
                    
                    <label class='checkcontainer ".($checked ? 'checkcontainer-checked' : '')."'>
                        {$label}
                        <input type='radio' name='{$name}' value='{$value}' ".($checked ? "checked='checked'": '').">
                    </label>
                ";
            },
        ]
    )->label('Сортировка:') ?>

    <?php ActiveForm::end() ?>
</div>

<?php foreach ($dataProvider->models as $model): ?>

    <div class="col-md-3 col-sm-4">
        <div class="panel panel-transparent profile-card">
            <div class="panel-body">
                <div class="row">
                        <?php
                            $user = \app\models\User::find()->where(['company_id' => $model['id'], 'role' => 2])->one();
                            if($user){
                                echo Html::a('<img class="img-content" src="/'.\app\helpers\AvatarResolver::getRealAvatarPath($user->avatar).'" alt="">', ['user/view', 'id' => $model['id']]);
                            } else {
                                echo Html::a('<img class="img-content" src="/'.\app\helpers\AvatarResolver::getRealAvatarPath(null).'" alt="">', ['user/view', 'id' => $model['id']]);
                            }
                        ?>
                        <div class="card-content">
                            <div class="card-content-text">
                                <h5><?= $model['company_name'] ?></h5>
                                <p>У<?=$model['level']?> чел. $<?= $model['credit'] ?></p>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

<?php endforeach; ?>

    <div class="col-md-12">
        <?= \yii\widgets\LinkPager::widget([
            'pagination' => $pages,
        ]) ?>
    </div>

<?php

$script = <<< JS

$('#user-main-form input').change(function(){
    $('#user-main-form').submit();
});

$('.card-content-text').equalHeights();

JS;

$this->registerJs($script, \yii\web\View::POS_READY);


?>