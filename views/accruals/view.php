<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Accruals */
?>
<div class="accruals-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'from_company_id',
            'for_company_id',
            'amount',
            'user_level',
        ],
    ]) ?>

</div>
