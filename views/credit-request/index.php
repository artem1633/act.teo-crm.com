<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Accruals;
use yii\bootstrap\Modal;

/** @var $this \yii\web\View */

$this->title = 'Вывод Credit';

$credit = 0;

if(Yii::$app->user->isGuest == false){
    $now = date('Y-m-d');
    $lastMonth = date('Y-m-d', time() - (86400 * 30));
    if(Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isManager()){
        $credit = array_sum(ArrayHelper::getColumn(Accruals::find()->where(['for_company_id' => Yii::$app->user->identity->company_id])->andWhere(['between', 'date', "{$lastMonth} 00:00:00", "{$now} 23:59:59"])->all(), 'amount'));
    }
}

\johnitvn\ajaxcrud\CrudAsset::register($this);

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title"></h4>
            </div>
            <div class="panel-body">
                <h3>Уважаемые партнеры!</h3>
                <p>
                    В настоящие время сервис по выводу бонусов осуществляет выплаты на банковские карты и расчетные счета РФ, Ведутся работы по восстановлению выплат в страны СНГ
                </p>
                <ul>
                    <li>Минимальный перевод бонуса 100 доллоров</li>
                    <li>Финансовый период по начислению бонусов: четверг 00:00 — среда 24:00</li>
                    <li>Закрытие недельного финансового периода происходит в каждую серду в 24:00</li>
                </ul>
                <p><a href="/test.pdf" download="Инструкция.pdf">Инструкция по выводу денег</a> <span class="text-danger">PDF</span></p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title"></h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <p style="font-size: 16px;">Общий баланс Credit: <?= $credit ?></p>
                    </div>
                    <div class="col-md-2">
                        <?= Html::a('Оставить заявку на вывод', ['credit-request/request'], ['class' => 'btn btn-warning', 'role' => 'modal-remote']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

