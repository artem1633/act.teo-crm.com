<?php

/** @var $this yii\web\View */
/** @var $report array */

use skeeks\widget\highcharts\Highcharts;
use yii\helpers\Html;

$this->title = 'Отчет';

// \yii\helpers\VarDumper::dump($report, 10, true);
// exit;

if(isset($report['data']) == false){
    $report['data'] = null;
}


?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Статистика</h4>
                </div>
                <div class="panel-body">
                    <?php \yii\widgets\Pjax::begin(['id' => 'pjax-chart-container']) ?>
                    <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'search-form', 'method' => 'GET']) ?>
                    <div class="row">
                        <div class="col-md-2">
                            <?= $form->field($searchModel, 'dateStart')->input('date') ?>
                        </div>
                        <div class="col-md-2">
                            <?= $form->field($searchModel, 'dateEnd')->input('date') ?>
                        </div>
                        <div class="col-md-2">
                            <?= Html::submitButton('Фильтровать', ['class' => 'btn btn-success btn-sm', 'style' => 'margin-top: 25px;']) ?>
                        </div>
                    </div>
                    <?php \yii\widgets\ActiveForm::end() ?>

                    <?= Highcharts::widget([
                        'options' => [
                            'chart' => [
                                'height' => 225,
                                'type' => 'line'

                            ],
                            'title' => false,//['text' => 'Всего отправлено сообщений'],
                            //'subtitle' => ['text' => 'Notice the difference between a 0 value and a null point'],
                            'plotOptions' => [
                                'column' => ['depth' => 25],
                                'line' => [
                                    'dataLabels' => [
                                        'enabled' => true
                                    ]
                                ],
                                'enableMouseTracking' => false,
                            ],
                            'xAxis' => [
                                'categories' => $subscribersReport['date'],
                                'labels' => [
                                    'skew3d' => true,
                                    'style' => ['fontSize' => '10px']
                                ]
                            ],
                            'yAxis' => [
                                'title' => ['text' => null]
                            ],
                            'series' => [
                                ['name' => 'Доход', 'data' => $subscribersReport['credit']],
                            ],
                            'credits' => ['enabled' => false],
                            'legend' => ['enabled' => true],
                        ]
                    ]);
                    ?>
                    <?php \yii\widgets\Pjax::end() ?>
                </div>
            </div>
        </div>
    </div>

    <?php if(Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isManager()): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Мой доход</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущая неделя</p>
                                <b> <?= $creditWeek ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущий месяц</p>
                                <b> <?= $creditMonth ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>За все время</p>
                                <b> <?= $creditAll ?> </b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Доход с линии Б</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущая неделя</p>
                                <b> <?= $creditWeekB ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущий месяц</p>
                                <b> <?= $creditMonthB ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>За все время</p>
                                <b> <?= $creditAllB ?> </b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Доход с линии C</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущая неделя</p>
                                <b> <?= $creditWeekC ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущий месяц</p>
                                <b> <?= $creditMonthC ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>За все время</p>
                                <b> <?= $creditAllC ?> </b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Доход с линии D</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущая неделя</p>
                                <b> <?= $creditWeekD ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущий месяц</p>
                                <b> <?= $creditMonthD ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>За все время</p>
                                <b> <?= $creditAllD ?> </b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php elseif(Yii::$app->user->identity->isShop()): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Доход с линии D</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущая неделя</p>
                                <b> <?= $debitWeek ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>Текущий месяц</p>
                                <b> <?= $debitMonth ?> </b>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="text-center" style="font-size: 18px;">
                                <p>За все время</p>
                                <b> <?= $debitAll ?> </b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>

<?php

$script = <<< JS
    $('#filter-form input').change(function(){
        $('#filter-form').submit();
    });
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>