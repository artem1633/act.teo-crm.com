<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "operations".
 *
 * @property int $id
 * @property string $name ФИО
 * @property string $order Продукт
 * @property double $sum Сумма
 * @property double $user_id Сумма
 * @property double $company_id Сумма
 * @property string $created_at Дата и время создания
 */
class Operations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operations';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'user_id',
                'value' => Yii::$app->user->getId(),
            ],
            [
                'class' => BlameableBehavior::class,
                'updatedByAttribute' => null,
                'createdByAttribute' => 'company_id',
                'value' => Yii::$app->user->identity->company_id,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sum', 'user_id', 'company_id'], 'number'],
            [['created_at'], 'safe'],
            [['name', 'order'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $company1 = Companies::findOne($this->company_id);

        if($company1){

            $rateA = $company1->rate_a;
            $rateB = $company1->rate_b;
            $rateC = $company1->rate_c;
            $rateD = $company1->rate_d;

//            var_dump((($this->sum * $rateA) / 100000));
//            exit;

            $company1->credit += ($this->sum * $rateA) / 100000;
            $company1->save(false);

            (new Accruals([
                'date' => date('Y-m-d H:i:s'),
                'from_company_id' => $company1->id,
                'for_company_id' => $company1->id,
                'amount' => (($this->sum * $rateA) / 100000),
                'user_level' => 1,
            ]))->save(false);

            if($company1->ref_id != null){
                $company2 = Companies::findOne($company1->ref_id);
                $company2->credit += ($this->sum * $rateB) / 100000;
                $company2->save(false);

                (new Accruals([
                    'date' => date('Y-m-d H:i:s'),
                    'from_company_id' => $company1->id,
                    'for_company_id' => $company2->id,
                    'amount' => ($this->sum * $rateB) / 100000,
                    'user_level' => 2,
                ]))->save(false);

                $company3 = Companies::findOne($company2->ref_id);
                if($company3){
                    $company3->credit = ($this->sum * $rateC) / 100000;
                    $company3->save(false);

                    (new Accruals([
                        'date' => date('Y-m-d H:i:s'),
                        'from_company_id' => $company2->id,
                        'for_company_id' => $company3->id,
                        'amount' => ($this->sum * $rateC) / 100000,
                        'user_level' => 3,
                    ]))->save(false);

                    $company4 = Companies::findOne($company3->ref_id);
                    if($company4){
                        $company4->credit = $this->sum * $rateD / 100000;
                        $company4->save(false);

                        (new Accruals([
                            'date' => date('Y-m-d H:i:s'),
                            'from_company_id' => $company3->id,
                            'for_company_id' => $company4->id,
                            'amount' => ($this->sum * $rateD) / 100000,
                            'user_level' => 4,
                        ]))->save(false);
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'order' => 'Продукт',
            'sum' => 'Сумма',
            'user_id' => 'Пользователь',
            'company_id' => 'Компания',
            'created_at' => 'Дата и время создания',
        ];
    }
}
