<?php

namespace app\models;

use app\components\user\Refer;
use app\helpers\AvatarResolver;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $login Логин
 * @property string $name ФИО
 * @property string $birth_date дата рождения
 * @property string $country страна проживания
 * @property string $living_place место проживания
 * @property string $education образование
 * @property string $profession профессия
 * @property string $work_place место работы
 * @property string $work_position должность
 * @property string $work_status статус работы
 * @property string $phone телефон
 * @property string $email почта
 * @property string $interests интересы
 * @property string $hobby хобби
 * @property string $password_hash Зашифрованный пароль
 * @property int $role Роль
 * @property double $balance_usd Баланс (Доллар США)
 * @property int $ref_id Кто пригласил
 * @property int $is_deletable Можно удалить или нельзя
 * @property string $last_active_datetime Дата и время последней активности
 * @property int $points баллы
 * @property string $created_at
 * @property string $avatar Аватар
 *
 * @property Refer $refer
 *
 * @property Accruals[] $accruals
 * @property RefAgent[] $refAgents
 * @property Ticket[] $tickets
 * @property Transaction[] $transactions
 * @property Transaction[] $transactions0
 * @property TransactionPurpose[] $transactionPurposes
 * @property User $ref
 * @property User[] $users
 * @property ChatHistory $last_message
 */
class User extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_DEFAULT = 'default';
    const SCENARIO_EDIT = 'edit';

    const ROLE_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_USER = 3;

    public $password;

    private $oldPasswordHash;


    /**
     * @var
     */
    private $_refer;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => null,
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'name',
                'login',
                'living_country',
                'is_deletable',
                'password',
                'password_hash',
                'role'
            ],
            self::SCENARIO_EDIT => [
                'name',
                'login',
                'is_deletable',
                'password',
                'password_hash',
                'phone',
                'role',
                'birth_date',
                'country',
                'living_place',
                'education',
                'profession',
                'work_place',
                'work_position',
                'interests',
                'living_country',
                'hobby',
                'work_status'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['password'], 'required', 'on' => self::SCENARIO_DEFAULT],
//            [
//                'password',
//                'match',
//                'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,50}$/',
//                'message' => '{attribute} не соответствует всем параметрам безопасности'
//            ],
            [['login'], 'unique'],
            ['login', 'email'],
            [['created_at', 'last_active_datetime'], 'safe'],
            [['is_deletable', 'rate_id'], 'integer'],
            [['balance_usd'], 'number'],
            [
                [
                    'login',
                    'name',
                    'country',
                    'living_place',
                    'education',
                    'profession',
                    'work_place',
                    'work_position',
                    'work_status',
                    'phone',
                    'email',
                    'interests',
                    'hobby',
                    'password_hash',
                    'living_country'
                ],
                'string',
                'max' => 255
            ],
            [
                ['ref_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['ref_id' => 'id']
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        $uid = Yii::$app->user->identity->id;

        if ($this->ref_id != null) {
            $users = User::find()->where(['ref_id' => $this->id])->all();

            foreach ($users as $user) {
                $user->ref_id = $this->ref_id;
                $user->save(false);
            }
        }

        if ($uid == $this->id) {
            Yii::$app->session->setFlash('error',
                "Вы авторизованы под пользователем «{$this->login}». Удаление невозможно!");
            return false;
        }

        if ($this->is_deletable == false) {
            Yii::$app->session->setFlash('error',
                "Этот пользователь не может подлежать удалению. Удаление невозможно!");
            return false;
        } else {
            return true;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->oldPasswordHash = $this->password_hash;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord){
            $this->role = 3;
            if(Yii::$app->user->isGuest == false){
                $this->company_id = Yii::$app->user->identity->company_id;
                $this->role = 3;
            } else if(Yii::$app->user->isGuest){
                $this->role = 2;
            }
        }
        if (parent::beforeSave($insert)) {


            if ($this->password != null) {
                $this->password_hash = Yii::$app->security->generatePasswordHash($this->password);
            } else {
                $this->password_hash = $this->oldPasswordHash;
            }

            return true;
        }
        return false;
    }

    /**
     * @return boolean
     */
    public function isSuperAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'name' => 'ФИО',
            'avatar' => 'Avatar',
            'birth_date' => 'дата рождения',
            'country' => 'Гражданство',
            'living_country' => 'Страна',
            'living_place' => 'Город',
            'education' => 'образование',
            'profession' => 'профессия',
            'work_place' => 'место работы',
            'work_position' => 'должность',
            'work_status' => 'статус работы',
            'phone' => 'телефон',
            'email' => 'почта',
            'interests' => 'интересы',
            'hobby' => 'хобби',
            'password_hash' => 'Зашифрованный пароль',
            'role' => 'Роль',
            'balance_usd' => 'Баланс (Доллар США)',
            'ref_id' => 'Кто пригласил',
            'is_deletable' => 'Можно удалить или нельзя',
            'last_active_datetime' => 'Дата и время последней активности',
            'points' => 'баллы',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * @return Refer
     */
    public function getRefer()
    {
        if ($this->_refer == null) {
            $this->_refer = new Refer(['user' => $this]);
        }

        return $this->_refer;
    }

    /**
     * @return string
     */
    public function getSelfRefHref()
    {
        return Url::toRoute(['site/register', 'ref_id' => $this->company_id], true);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTickets()
    {
        return $this->hasMany(Ticket::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRef()
    {
        return $this->hasOne(User::className(), ['id' => 'ref_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccrualsFor()
    {
        return $this->hasMany(Accruals::className(), ['for_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccrualsFrom()
    {
        return $this->hasMany(Accruals::className(), ['from_user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['ref_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * @return string
     */
    public function getRealAvatarPath()
    {
        return AvatarResolver::getRealAvatarPath($this->avatar);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['login' => $username]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getPermmission()
    {
        return $this->stat_indet;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->password;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return boolean
     */
    public function isOnline()
    {
        if ($this->last_active_datetime == null) {
            return false;
        }

        $nowDt = new \DateTime("now", new \DateTimeZone(\Yii::$app->timeZone));
        $seen = new \DateTime($this->last_active_datetime, new \DateTimeZone(\Yii::$app->timeZone));

        return $nowDt->getTimestamp() - $seen->getTimestamp() < 2 * 60;
    }

    /**
     * @return User[]
     */
    public function getChatPartners()
    {
        return self::find()->andWhere(['<>', 'id', Yii::$app->user->identity->id])->all();
    }

    /**
     * @param int $id Идентификатор пользователя
     * @return mixed
     */
    public function getLastMessage($id)
    {
        /** @var ChatHistory $last_message */
        $last_message = ChatHistory::find()->andWhere(['sender_id' => $id])->orderBy(['id' => SORT_DESC])->one() ?? null;
        if (!$last_message) {
            return [
                'message' => '',
                'date' => '',
            ];
        }
        $last_message->text = strip_tags($last_message->text);
        return [
            'message' => mb_substr($last_message->text, 0, 20, 'UTF-8') . '...',
            'date' => $last_message->message_send_datetime,
        ];
    }

    /**
     * @param int $id Идентификатор пользователя
     * @return mixed
     */
    public function getLastMessageChat($id)
    {
        $pks = [Yii::$app->user->getId(), $id];
        /** @var ChatHistory $last_message */
        $last_message = ChatHistory::find()->andWhere(['and', ['recipient_id' => $pks], ['sender_id' => $pks]])->orderBy(['id' => SORT_DESC])->one() ?? null;
        if (!$last_message) {
            return [
                'message' => '',
                'date' => '1970-01-01 00:00:00',
            ];
        }
        $last_message->text = strip_tags($last_message->text);
        return [
            'message' => mb_substr($last_message->text, 0, 20, 'UTF-8') . '...',
            'date' => $last_message->message_send_datetime,
        ];
    }

    /**
     * Получает кол-во непрочитанных сообщений
     * @return int|string
     */
    public function getUnreadMessagesCount()
    {
        return ChatHistory::find()->andWhere([
            'sender_id' => $this->id,
            'recipient_id' => Yii::$app->user->identity->id ,
            'read' => 0
        ])->count();
    }

    /**
     * Получает статус пользователя
     * @return string
     */
    public function getStatus()
    {
        $last_active = time() - strtotime($this->last_active_datetime);

        if ($last_active < (5 * 60)){
            $class = 'partner-status green-status';
        } elseif ($last_active > (5 * 60) && $last_active < (30 * 60)){
            $class = 'partner-status yellow-status';
        } else {
            $class = 'partner-status red-status';
        }

        return $class;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function isManager()
    {
        if ($this->role == 2) {
            return true;
        }
        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function isShop()
    {
        if ($this->role == 3) {
            return true;
        }
        return false;
    }
}
