<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "companies".
 *
 * @property int $id
 * @property string $company_name Название компании
 * @property string $created_date Дата регистрации
 * @property double $rate_a Тариф A
 * @property double $rate_b Тариф B
 * @property double $rate_c Тариф C
 * @property double $rate_d Тариф D
 * @property double $credit Кредитов
 * @property string $last_activity_datetime Дата и время последней активности
 * @property string $access_end_datetime Дата и время потери доступа к системе
 * @property int $is_super Супер компания
 * @property int $is_activity Доступ
 *
 * @property Companies $ref
 * @property Companies[] $companies
 */
class Companies extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'companies';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_date', 'last_activity_datetime', 'access_end_datetime'], 'safe'],
            [['rate_a', 'rate_b', 'rate_c', 'rate_d', 'credit'], 'number'],
            [['is_super', 'is_activity', 'ref_id'], 'integer'],
            [['company_name'], 'string', 'max' => 255],
            [['ref_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['ref_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_name' => 'Название компании',
            'created_date' => 'Дата регистрации',
            'rate_a' => 'Тариф A',
            'rate_b' => 'Тариф B',
            'rate_c' => 'Тариф C',
            'rate_d' => 'Тариф D',
            'credit' => 'Кредитов',
            'last_activity_datetime' => 'Дата и время последней активности',
            'access_end_datetime' => 'Дата и время потери доступа к системе',
            'is_super' => 'Супер компания',
            'is_activity' => 'Доступ',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRef()
    {
        return $this->hasOne(Companies::className(), ['id' => 'ref_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['ref_id' => 'id']);
    }
}
