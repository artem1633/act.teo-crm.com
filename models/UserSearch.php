<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;
use yii\data\ArrayDataProvider;
use yii\data\Sort;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    const SORTING_REF_COUNT = 1;
    const SORTING_BALANCE = 2;
    const SORTING_DATE = 3;
    const SORTING_COUNTRY = 4;
    const SORTING_CITY = 5;
    const SORTING_LEADER = 6;


    public $level;

    public $sorting;

    public $sortingRight;

    private $parentPks = [];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_deletable', 'ref_id', 'level', 'sorting', 'sortingRight'], 'integer'],
            [['balance_usd'], 'number'],
            [['login', 'name', 'password_hash'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ref_id' => $this->ref_id,
            'balance_usd' => $this->balance_usd,
            'is_deletable' => $this->is_deletable,
        ]);

        $query->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash]);

        if (!Yii::$app->user->identity->isSuperAdmin()){
            $query->andFilterWhere(['=', 'company_id', Yii::$app->user->identity->company_id]);
        }
//        $query->andWhere(['id' => $pks]);

        return $dataProvider;
    }

    /**
     * @param $user User
     * @return bool|string
     */
    public function searchChild($params)
    {
        $user = Yii::$app->user->identity;
        $userLevel1Count = 0;
        $userLevel2Count = 0;
        $userLevel3Count = 0;
        $userLevel4Count = 0;
        $userLevel5Count = 0;
        $userLevel6Count = 0;
        $userLevel7Count = 0;

        $this->load($params);

        if($this->sortingRight == self::SORTING_LEADER){
            $userLevel1Count = Companies::find()->count();
            $userLevel1 = Companies::find()->all();
        } else {
            $userLevel1Count = Companies::find()->where(['ref_id' => $user->company_id])->count();
            $userLevel1 = Companies::find()->where(['ref_id' => $user->company_id])->all();
        }

        $pks = [];
        $pks1 = [];
        $pks2 = [];
        $pks3 = [];
        $pks4 = [];
        $pks5 = [];
        $pks6 = [];
        $pks7 = [];

        if ($userLevel1) {

            $pks = ArrayHelper::getColumn($userLevel1, 'id');
            $pks1 = ArrayHelper::getColumn($userLevel1, 'id');

            foreach ($userLevel1 as $itemUserLevel1) {
//                $userLevel2Count += User::find()->where(['ref_id' => $itemUserLevel1->id])->count();
                $userLevel2 = Companies::find()->where(['ref_id' => $itemUserLevel1->id])->all();


                if ($userLevel2) {

                    $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel2, 'id'));
                    $pks2 = ArrayHelper::getColumn($userLevel2, 'id');

                    foreach ($userLevel2 as $itemUserLevel2) {
//                        $userLevel3Count += User::find()->where(['ref_id' => $itemUserLevel2->id])->count();
                        $userLevel3 = Companies::find()->where(['ref_id' => $itemUserLevel2->id])->all();
                        if ($userLevel3) {

                            $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel3, 'id'));
                            $pks3 = ArrayHelper::getColumn($userLevel3, 'id');

                            foreach ($userLevel3 as $itemUserLevel3) {
//                                $userLevel4Count += User::find()->where(['ref_id' => $itemUserLevel3->id])->count();
                                $userLevel4 = Companies::find()->where(['ref_id' => $itemUserLevel3->id])->all();
                                if (count($userLevel4) > 0) {

                                    $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel4, 'id'));
                                    $pks4 = ArrayHelper::getColumn($userLevel4, 'id');

//                                    foreach ($userLevel4 as $itemUserLevel4) {
////                                        $userLevel5Count += User::find()->where(['ref_id' => $itemUserLevel4->id])->count();
//                                        $userLevel5 = User::find()->where(['ref_id' => $itemUserLevel4->id])->all();
//
//                                        if($userLevel5){
//
//                                            $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel5, 'id'));
//                                            $pks5 = ArrayHelper::getColumn($userLevel5, 'id');
//
//                                            foreach ($userLevel5 as $itemUserLevel5){
//
////                                                $userLevel6Count += User::find()->where(['ref_id' => $itemUserLevel5->id])->count();
//                                                $userLevel6 = User::find()->where(['ref_id' => $itemUserLevel5->id])->all();
//
//                                                if($userLevel6) {
//
//                                                    $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel6, 'id'));
//                                                    $pks6 = ArrayHelper::getColumn($userLevel6, 'id');
//
//                                                    foreach ($userLevel6 as $itemUserLevel6){
//
////                                                        $userLevel7Count += User::find()->where(['ref_id' => $itemUserLevel6->id])->count();
//                                                        $userLevel7 = User::find()->where(['ref_id' => $itemUserLevel6->id])->all();
//
//                                                        if($userLevel7) {
//
//                                                            $pks = ArrayHelper::merge($pks, ArrayHelper::getColumn($userLevel7, 'id'));
//                                                            $pks7 = ArrayHelper::getColumn($userLevel7, 'id');
//
//                                                        }
//
//                                                    }
//
//                                                }
//
//                                            }
//                                        }
//
//                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $query = Companies::find();

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ref_id' => $this->ref_id,
            'balance_usd' => $this->balance_usd,
            'is_deletable' => $this->is_deletable,
        ]);

        $query->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash]);

        $query->andWhere(['id' => $pks]);

        $query->asArray(true);

        $models = $query->all();

        for ($i = 0; $i < count($models); $i++)
        {
            $model = $models[$i];

            if(in_array($model['id'], $pks1)){
                $models[$i]['level'] = 1;
            } else if(in_array($model['id'], $pks2)){
                $models[$i]['level'] = 2;
            } else if(in_array($model['id'], $pks3)){
                $models[$i]['level'] = 3;
            } else if(in_array($model['id'], $pks4)){
                $models[$i]['level'] = 4;
            } else {
                $models[$i]['level'] = null;
            }

            $user = Companies::findOne($models[$i]['id']);
//            $models[$i]['count_ref'] = $user->refer->getTotalCount();
            $models[$i]['count_ref'] = 0;

//            if($this->level != null && $models[$i]['level'] != $this->level){
//                unset($models[$i]);
//            }

        }


        if($this->level != null){
            $models = array_filter($models, function($model){
                return $model['level'] == $this->level;
            });
        }

        if($this->sortingRight == self::SORTING_LEADER){
            uasort($models, function($a, $b){
                return $a['balance_usd'] < $b['balance_usd'];
            });
            uasort($models, function($a, $b){
                return $a['count_ref'] < $b['count_ref'];
            });
            if(count($models) > 50){
                $models = array_slice($models, 0, 50);
            }
        }

        if($this->sorting == self::SORTING_REF_COUNT){
            uasort($models, function($a, $b){
                return $a['count_ref'] < $b['count_ref'];
            });
        }
        if($this->sorting == self::SORTING_BALANCE){
            uasort($models, function($a, $b){
                return $a['balance_usd'] < $b['balance_usd'];
            });
        }
        if($this->sorting == self::SORTING_DATE){
            uasort($models, function($a, $b){
                return strtotime($a['birth_date']) < strtotime($b['birth_date']);
            });
        }
        if($this->sorting == self::SORTING_COUNTRY){
//            uasort($models, function($a, $b){
//                $la = mb_substr($a['country'],0,1,'utf-8');
//                $lb = mb_substr($b['country'],0,1,'utf-8');
//                if(ord($la) > 122 && ord($lb) > 122){
//                    return $a['country'] > $b['country'] ? 1 : -1;
//                }
//                if(ord($la) > 122 || ord($lb) > 122) {
//                    return $a['country'] < $b['country'] ? 1 : -1;
//                }
//            });
            ArrayHelper::multisort($models, ['country'], [SORT_ASC]);
        }
        if($this->sorting == self::SORTING_CITY){
//            uasort($models, function($a, $b){
//                $la = mb_substr($a['living_place'],0,1,'utf-8');
//                $lb = mb_substr($b['living_place'],0,1,'utf-8');
//                if(ord($la) > 122 && ord($lb) > 122){
//                    return $a['living_place'] > $b['living_place'] ? 1 : -1;
//                }
//                if(ord($la) > 122 || ord($lb) > 122) {
//                    return $a['living_place'] < $b['living_place'] ? 1 : -1;
//                }
//            });
            ArrayHelper::multisort($models, ['living_place'], [SORT_ASC]);
        }



//        VarDumper::dump($models, 10, true);
//        exit;

//        $data = [
//            [
//                'value' => 'А'
//            ],
//            [
//                'value' => 'В'
//            ],
//            [
//                'value' => 'Е'
//            ],
//            [
//                'value' => 'Б'
//            ],
//            [
//                'value' => 'Г'
//            ],
//            [
//                'value' => 'Д'
//            ],
//        ];

//        ArrayHelper::multisort($models, ['living_place'], [SORT_ASC]);

//        VarDumper::dump($models, 10, true);
//        exit;

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'pageSize' => 50,
            ],
        ]);

        return $dataProvider;
    }

    public function levelRecursive($user, $counter = 1, $pks = [])
    {
        $parentUser = User::find()->where(['id' => $user->ref_id])->one();

        if($parentUser){

            $this->parentPks = ArrayHelper::merge($pks, [$parentUser->id]);

            $this->levelRecursive($parentUser, $counter+1, $this->parentPks);
        }
    }

    /**
     * @param $user User
     * @return bool|string
     */
    public function searchParent($params)
    {
        $user = Yii::$app->user->identity;
        $userLevel1Count = 0;
        $userLevel2Count = 0;
        $userLevel3Count = 0;
        $userLevel4Count = 0;
        $userLevel5Count = 0;
        $userLevel6Count = 0;
        $userLevel7Count = 0;
        $userLevel1Count = User::find()->where(['id' => $user->ref_id])->count();
        $userLevel1 = User::find()->where(['id' => $user->ref_id])->all();

        $pks = [];
        $pks1 = [];
        $pks2 = [];
        $pks3 = [];
        $pks4 = [];
        $pks5 = [];
        $pks6 = [];
        $pks7 = [];



        $query = User::find();

        $dataProvider = new ArrayDataProvider();

        $this->load($params);

        if($this->sortingRight == self::SORTING_LEADER){
            $pks = ArrayHelper::getColumn(User::find()->all(), 'id');
        } else {
            $this->levelRecursive($user);
            $pks = $this->parentPks;
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ref_id' => $this->ref_id,
            'balance_usd' => $this->balance_usd,
            'is_deletable' => $this->is_deletable,
        ]);

        $query->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash]);

        $query->andWhere(['id' => $pks]);

        $query->asArray(true);

        $models = $query->all();

        for ($i = 0; $i < count($models); $i++)
        {
            $models[$i]['level'] = $i;

            $user = User::findOne($models[$i]['id']);
            $models[$i]['count_ref'] = $user->refer->getTotalCount();

//            if($this->level != null && $models[$i]['level'] != $this->level){
//                unset($models[$i]);
//            }

        }

        if($this->level != null){
            $models = array_filter($models, function($model){
                return $model['level'] == $this->level;
            });
        }

        if($this->sorting == self::SORTING_REF_COUNT){
            uasort($models, function($a, $b){
                return $a['count_ref'] < $b['count_ref'];
            });
        }
        if($this->sorting == self::SORTING_BALANCE){
            uasort($models, function($a, $b){
                return $a['balance_usd'] < $b['balance_usd'];
            });
        }
        if($this->sorting == self::SORTING_DATE){
            uasort($models, function($a, $b){
                return strtotime($a['birth_date']) < strtotime($b['birth_date']);
            });
        }
        if($this->sorting == self::SORTING_COUNTRY){
//            uasort($models, function($a, $b){
//                $la = mb_substr($a['country'],0,1,'utf-8');
//                $lb = mb_substr($b['country'],0,1,'utf-8');
//                if(ord($la) > 122 && ord($lb) > 122){
//                    return $a['country'] > $b['country'] ? 1 : -1;
//                }
//                if(ord($la) > 122 || ord($lb) > 122) {
//                    return $a['country'] < $b['country'] ? 1 : -1;
//                }
//            });
            ArrayHelper::multisort($models, ['country'], [SORT_ASC]);
        }
        if($this->sorting == self::SORTING_CITY){
//            uasort($models, function($a, $b){
//                $la = mb_substr($a['living_place'],0,1,'utf-8');
//                $lb = mb_substr($b['living_place'],0,1,'utf-8');
//                if(ord($la) > 122 && ord($lb) > 122){
//                    return $a['living_place'] > $b['living_place'] ? 1 : -1;
//                }
//                if(ord($la) > 122 || ord($lb) > 122) {
//                    return $a['living_place'] < $b['living_place'] ? 1 : -1;
//                }
//            });
            ArrayHelper::multisort($models, ['living_place'], [SORT_ASC]);
        }

        $dataProvider->setModels($models);

        return $dataProvider;
    }

    /**
     * @param array $params
     * @return ArrayDataProvider
     */
    public function searchByRating($params = [])
    {
        $query = (new Query())
            ->select([
                'user.*',
            ])
            ->from('user');

        $dataProvider = new ArrayDataProvider();

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'ref_id' => $this->ref_id,
            'balance_usd' => $this->balance_usd,
            'is_deletable' => $this->is_deletable,
        ]);

        $query->andFilterWhere(['like', 'login', $this->login])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash]);


        $models = $query->all();

        for($i = 0; $i < count($models); $i++)
        {
            $user = User::findOne($models[$i]['id']);
            $models[$i]['count_ref'] = $user->refer->getTotalCount();
        }


        if($this->sorting == self::SORTING_REF_COUNT){
            uasort($models, function($a, $b){
                return $a['count_ref'] < $b['count_ref'];
            });
        }
        if($this->sorting == self::SORTING_BALANCE){
            uasort($models, function($a, $b){
                return $a['balance_usd'] < $b['balance_usd'];
            });
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
        ]);

        $dataProvider->setModels($models);

        return $dataProvider;
    }

}
